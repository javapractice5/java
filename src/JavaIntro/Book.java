package JavaIntro;

public class Book {
    int noOfCopies;

    public Book(int noOfCopies)
    {
        this.noOfCopies=noOfCopies;
    }
    void setNoOfCopies(int noOfCopies)
    {   if(noOfCopies>0)
            this.noOfCopies=noOfCopies;
    }
    int getNoOfCopies()
    {
        return this.noOfCopies;
    }
    void increaseNoOfCopies(int howMuch)
    {
        setNoOfCopies(this.noOfCopies+ howMuch);
    }

    void decreaseNoOfCopies(int howMuch)
    {
        setNoOfCopies(this.noOfCopies- howMuch);
    }

    void author()
    {
        System.out.println("Author is different");
    }

}
