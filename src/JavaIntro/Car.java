package JavaIntro;

public class Car {
    int speed;

    public Car(int speed)
    {
        this.speed=speed;
    }
    void speed(int speed)
    {   if(speed>0)
        this.speed=speed;
    }
    int getSpeed()
    {
        return this.speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    void increaseSpeed(int howMuch)
    {
        setSpeed(this.speed+ howMuch);
    }

    void decreaseSpeed(int howMuch)
    {
        setSpeed(this.speed- howMuch);
    }

    void start()
    {
        System.out.println("Started");
    }
}
