package JavaIntro;

public class CarRunner {
    public static void main(String args[]) {
        Car BMW = new Car(100);
        Car Jaguar = new Car(200);
        Car Audi = new Car(50);

        System.out.println(BMW.getSpeed());
        System.out.println(Jaguar.getSpeed());
        System.out.println(Audi.getSpeed());
        BMW.start();
        Jaguar.start();
        Audi.start();

        BMW.setSpeed(200);
        Jaguar.setSpeed(500);
        Audi.setSpeed(100);

        BMW.increaseSpeed(100);
        BMW.decreaseSpeed(20);

        System.out.println(BMW.getSpeed());
        System.out.println(Jaguar.getSpeed());
        System.out.println(Audi.getSpeed());
    }
}
