package JavaIntro;

public class BookRunner {
    public static void main(String args[]) {
        Book artOfComputerProgramming = new Book(100);
        Book effectiveJava = new Book(200);
        Book cleanCode = new Book(50);

        System.out.println(artOfComputerProgramming.getNoOfCopies());
        System.out.println(effectiveJava.getNoOfCopies());
        System.out.println(cleanCode.getNoOfCopies());
        artOfComputerProgramming.author();
        effectiveJava.author();
        cleanCode.author();

        artOfComputerProgramming.setNoOfCopies(200);
        effectiveJava.setNoOfCopies(500);
        cleanCode.setNoOfCopies(100);

        artOfComputerProgramming.increaseNoOfCopies(100);
        artOfComputerProgramming.decreaseNoOfCopies(20);

        System.out.println(artOfComputerProgramming.getNoOfCopies());
        System.out.println(effectiveJava.getNoOfCopies());
        System.out.println(cleanCode.getNoOfCopies());
    }
}
